const Generator = require('yeoman-generator');
const fs = require('fs');
const path = require('path');
const process = require('process');

module.exports = class extends Generator {
    async prompting() {
        this.answers = await this.prompt([{
            type: "input",
            name: "name",
            message: "你的工程项目名称？"
        }])
    }
    writing() {
        this.projectPath = path.join(process.cwd(), this.answers.name)
        if (!fs.existsSync(this.projectPath)) {
            fs.mkdirSync(this.projectPath)
        }
        const pkgJson = {
            name: this.answers.name,
            version: "1.0.0",
            description: "",
            main: "app/index.jsx",
            scripts: {
                "test": "eslint app/**",
                "build": "webpack --colors --progress --profile",
                "start": "webpack-dev-server"
            },
            repository: {},
            keywords: [],
            author: "",
            license: "ISC",
            devDependencies: {
                "@types/qs": "^6.9.7",
                "@types/react": "^17.0.19",
                "@types/react-dom": "^17.0.9",
                "@types/react-router-dom": "^5.1.8",
                "autoprefixer": "^10.3.3",
                "css-loader": "^6.2.0",
                "html-webpack-plugin": "^5.3.2",
                "mini-css-extract-plugin": "^2.2.0",
                "postcss": "^8.3.6",
                "postcss-loader": "^6.1.1",
                "postcss-pxtorem": "^6.0.0",
                "style-loader": "^3.2.1",
                "terser-webpack-plugin": "^5.1.4",
                "ts-loader": "^9.2.5",
                "typescript": "^4.4.2",
                "webpack": "^5.51.1",
                "webpack-cli": "^4.8.0",
                "webpack-dev-server": "^4.0.0"
            },
            dependencies: {
                "@reduxjs/toolkit": "^1.6.1",
                "axios": "^0.21.1",
                "qs": "^6.10.3",
                "react": "^17.0.2",
                "react-dom": "^17.0.2",
                "react-redux": "^7.2.5",
                "react-router-dom": "^5.2.1"
            }
        };

        this.fs.extendJSON(this.destinationPath(path.join(this.projectPath, "package.json")), pkgJson);
        this.fs.copyTpl(
            this.templatePath('tsconfig.json'),
            this.destinationPath(path.join(this.projectPath, './tsconfig.json'))
        )
        this.fs.copyTpl(
            this.templatePath('.eslintignore'),
            this.destinationPath(path.join(this.projectPath, './.eslintignore'))
        )
        this.fs.copyTpl(
            this.templatePath('.eslintrc.json'),
            this.destinationPath(path.join(this.projectPath, './.eslintrc.json'))
        )
        this.fs.copyTpl(
            this.templatePath('.prettierrc.json'),
            this.destinationPath(path.join(this.projectPath, './.prettierrc.json'))
        )
        this.fs.copyTpl(
            this.templatePath('webpack.config.js'),
            this.destinationPath(path.join(this.projectPath, './webpack.config.js'))
        )
        this.fs.copyTpl(
            this.templatePath('src'),
            this.destinationPath(path.join(this.projectPath, "./app"))
        )
    }
    install() {
        this.npmInstall('', {}, {
            cwd: this.destinationPath(this.projectPath)
        });
    }
}