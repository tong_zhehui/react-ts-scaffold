/* eslint-disable no-use-before-define */
import React, { Component } from 'react';

export default (fn: Function) => class extends Component<{}, { component?: any }> {
  constructor(props: {}) {
    super(props);
    this.state = {
      component: null,
    };
  }

  async componentDidMount() {
    const { default: asyncComponent } = await fn();
    this.setState({
      component: asyncComponent,
    });
  }

  render() {
    const { state: { component: C }, props } = this;
    return C ? <C {...props} /> : null;
  }
};
