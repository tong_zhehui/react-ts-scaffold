import axios, {
  AxiosRequestConfig,
  AxiosResponse,
  CancelTokenSource,
} from 'axios';

const instance = axios.create();
const cancleMap = new Map<string, CancelTokenSource>();

instance.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    if (cancleMap.has(`${config.url}-${config.method}`)) {
      const cancle: CancelTokenSource = cancleMap.get(
        `${config.url}-${config.method}`,
      );
      cancle.cancel('重复提交');
      cancleMap.delete(`${config.url}-${config.method}`);
    }
    const source: CancelTokenSource = axios.CancelToken.source();
    config.cancelToken = source.token;
    cancleMap.set(`${config.url}-${config.method}`, source);

    return config;
  },
  (error) => Promise.reject(error),
);

instance.interceptors.response.use((response: AxiosResponse) => {
  cancleMap.delete(`${response.config.url}-${response.config.method}`)
  console.log('response:', response);
  return response;
});

export default instance;
