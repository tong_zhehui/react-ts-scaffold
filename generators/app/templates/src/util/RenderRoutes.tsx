/* eslint-disable no-use-before-define */
import React, { ComponentType, ReactElement } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';

export type RouteConfigProps = RouteProps & { route: RouteConfig } & any;

export interface RouteConfig {
  key?: React.Key;
  path: string;
  component?: ComponentType<RouteConfigProps>;
  redirect?: string;
  exact?: boolean;
  strict?: boolean;
  routes?: RouteConfig[];
  [propName: string]: any;
}

export const renderRoutes = (routes: RouteConfig[]): ReactElement[] => {
  const routers = routes.map((item, index) => {
    const Component = item.component;
    if (item.redirect) {
      return (
        <Redirect
          key={index}
          from={item.path}
          to={item.redirect}
        />
      );
    }
    return (
      <Route
        key={index}
        path={item.path}
        exact={!!item.exact}
        render={(props: RouteProps | any) => <Component {...props} route={item} />}
      />
    );
  });

  return routers;
};
