/* eslint-disable no-use-before-define */
import React from 'react';
import ReactDom from 'react-dom';
import AppRouter from './routes/index';
import '@css/common.css';

ReactDom.render(<AppRouter />, document.getElementById('app'));
