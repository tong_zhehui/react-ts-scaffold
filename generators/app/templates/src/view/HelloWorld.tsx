/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-use-before-define
import React, { Component } from 'react';
import { renderRoutes, RouteConfigProps } from '../util/RenderRoutes';

export default (props: RouteConfigProps) => {
  const { route: { routes } } = props;
  return (
    <div>
      <p>hello world tzh !!</p>
      {renderRoutes(routes)}
    </div>
  );
};
