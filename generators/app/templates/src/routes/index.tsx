/* eslint-disable no-use-before-define */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect, Component } from 'react';
import {
  BrowserRouter as Router,
  HashRouter,
} from 'react-router-dom';
import lazy from '../util/LazyComponent';
import { renderRoutes, RouteConfig } from '../util/RenderRoutes';

const HelloWorld = lazy(() => import('@view/HelloWorld'));

const routes: RouteConfig[] = [
  {
    path: '/',
    exact: true,
    redirect: '/hello',
  },
  {
    path: '/hello',
    component: HelloWorld,
    routes: [
      {
        path: '/hello/test',
        component: () => <div>fdsfs</div>,
      },
    ],
  },
];

export default () => (
  <HashRouter>
    <HashRouter>{renderRoutes(routes)}</HashRouter>
  </HashRouter>
);
