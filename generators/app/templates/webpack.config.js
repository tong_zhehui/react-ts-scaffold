/* eslint-disable no-undef */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const pxtorem = require('postcss-pxtorem');

module.exports = {
  mode: 'development',
  entry: path.join(__dirname, 'app', 'index.tsx'),
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'static/js/bundle.js',
    clean: true
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        include: [path.resolve(__dirname, 'app')],
        exclude: [path.resolve(__dirname, 'node_modules')],
        use: 'ts-loader'
      },
      {
        test: /\.css$/,
        include: [path.resolve(__dirname, 'node_modules')],
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ]
      },
      {
        test: /\.css$/,
        include: [path.resolve(__dirname, 'app')],
        exclude: [path.resolve(__dirname, 'node_modules')],
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  autoprefixer(['last 2 version', '> 1%', 'not ie < 11']),
                  pxtorem({
                    rootValue: 16,
                    unitPrecision: 5,
                    propList: [
                      'font',
                      'font-size',
                      'line-height',
                      'letter-spacing',
                      'width',
                      'max-width',
                      'min-width',
                      'height',
                      'max-height',
                      'min-height',
                      'margin',
                      'padding',
                      '*'
                    ],
                    selectorBlackList: [],
                    replace: true,
                    mediaQuery: false,
                    minPixelValue: 0,
                    exclude: /node_modules/i,
                  }),
                ],
              },
            },
          },
        ],
      },

      {
        test: /\.(png|svg|jpg|gif)$/,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 4 * 1024 // 4kb
          }
        },
        generator: {
          filename: 'static/images/[hash][ext][query]'
        }
      },
    ],
  },
  resolve: {
    extensions: ['.json', '.ts', '.tsx', '.js', '.jsx'],
    alias: {
      '@': path.resolve('./app'),
      '@img': path.resolve('./app/images'),
      '@css': path.resolve('./app/css'),
      '@view': path.resolve('./app/view'),
      '@components': path.resolve('./app/components'),
      '@api': path.resolve('./app/api'),
      '@util': path.resolve('./app/util'),
    },
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'static/css/[name].css',
      chunkFilename: 'static/css/[id].css',
    }),
    new HtmlWebpackPlugin({
      template: './app/index.html',
      filename: 'index.html',
    }),
  ],
  performance: {
    hints: 'warning',
    // 入口起点的最大体积 整数类型（以字节为单位）
    maxEntrypointSize: 50000000,
    // 生成文件的最大体积 整数类型（以字节为单位 300k）
    maxAssetSize: 50000000,
    // 只给出 js 文件的性能提示
    assetFilter(assetFilename) {
      return assetFilename.endsWith('.js');
    },
  },
  optimization: {
    splitChunks: {
      chunks: "all",
      automaticNameDelimiter: '~',
      cacheGroups: {
        //第三方库抽离
        vendor: {
          name: "modules",
          test: /[\\/]node_modules[\\/]/,
          priority: -10
        },
      }
    }
  },
  devServer: {
    hot: true
  },
  cache: true
};
